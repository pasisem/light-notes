import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import * as M from 'materialize-css';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title = "Notes"
  private sub: object;
  private notes: Note[];
  private newNote: Note;
  private selectedNote: Note;
  private editing: boolean;
  private reverseOrder: boolean;
  private loading: boolean;

  constructor(private data:DataService) { }

  ngOnInit() {
    this.notes = [];
    this.editing = false;
    this.unselectNote();
    this.emptyForm();
    this.reverseOrder = false;

    this.getNotes(this.reverseOrder);

    // materialize modal popup
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.modal');
      var instances = M.Modal.init(elems, {});
    });
    // materialize sidenav
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems, {});
    });
  }
  selectNote(note:Note){
    this.selectedNote = {
      _id: note._id,
      text: note.text,
      name: note.name,
      created: note.created,
      updated: note.updated
    };
  }
  selectNoteEdit(note:Note){
    this.editing = true;
    this.selectNote(note);
  }
  unselectNote(){
    this.editing = false;
    this.selectedNote = null;
  }
  emptyForm(){
    this.newNote = {
      _id: null,
      text: null,
      name: null, 
      created: null,
      updated: null
    };
  }

  // markup helpers
  editingCheck(note:Note){
    if(!this.editing || !this.selectedNote || note._id != this.selectedNote._id){
      return false;
    } else {
      return true;
    }
  }
  dateToLocal(dateString){
    let newDate = new Date(parseInt(dateString));
    return newDate.toLocaleTimeString() + " - " + newDate.toLocaleDateString();
  }

  // data
  getNotes(reverse:boolean){
    this.loading = true;
    this.data.getNotes(reverse).subscribe((data) => {
      this.loading = false;
      this.notes = data;
    });
  }
  addNote(){
    this.loading = true;
    this.data.addNote(this.newNote).subscribe(note => {
      this.getNotes(this.reverseOrder);
      this.emptyForm();
    });
  }
  editNote(note:Note){
    this.loading = true;
    this.data.updateNote(note).subscribe(note => {
      this.unselectNote();
      this.getNotes(this.reverseOrder);
    });
  }
  deleteNote(id){
    this.loading = true;
    this.data.deleteNote(id).subscribe(note => {
      this.getNotes(this.reverseOrder);
    });
  }
}
interface Note{
  _id: string,
  text: string,
  name: string, 
  created: string, 
  updated: string
}