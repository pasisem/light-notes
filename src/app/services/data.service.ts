import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private url:string;
  private httpOptions:object;

  constructor(private http:HttpClient) {
    // this.url = "http://localhost:3000/api";
    this.url = "/api";

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
   }


  getNotes(reverse){
    return this.http.get( this.url + '/notes').pipe(map((res: any) => {
      if (reverse){
        return res.reverse();
      } else {
        return res;
      }
    }));
  }

  getNote(id){
    return this.http.get( this.url + '/note/'+id).pipe(map((res: any) => {
      return res;
    }));
  }

  addNote(newNote){

    return this.http.post( this.url + '/note', JSON.stringify(newNote), this.httpOptions).pipe(map((res: any) => {
      return res;
    }));
  }

  updateNote(updatedNote){
    return this.http.put( this.url + '/note/'+updatedNote._id, JSON.stringify(updatedNote), this.httpOptions).pipe(map((res: any) => {
      return res;
    }));
  }

  deleteNote(id){
    console.log(id);
    return this.http.delete( this.url + '/note/'+id).pipe(map((res: any) => {
      return res;
    }));
  }
}
